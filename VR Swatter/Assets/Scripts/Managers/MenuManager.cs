﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using System;

public class MenuManager : MonoBehaviour
{
    enum MenuState { Settings, Lobby, MainMenu };
    public Animator VRUIAnimator;
    public Text PressXToJoinText;
    private MusicManager musicManager;
    private Text text;
    private int numPlayers = 1;

    private bool xAxisInUse = false;
    private bool yAxisInUse = false;
    enum MenuSelectStates { StartLobby, HowToPlay, Quit };
    private MenuSelectStates currentMenuSetting = MenuSelectStates.StartLobby;
    public GameObject MainMenuCanvas;
    public GameObject HowToPlayCanvas;

    public GameObject[] players;
    public GameObject LobbyCanvas;
    public GameObject InvertCanvas;
    public GameObject SettingsCanvas;
    public GameObject Leaderboard;
    public GameObject VRLeaderboard;
    public GameObject featherPickUp;

    private List<GameObject> _activePlayers;

    private float _startGameTimer = 9f;
    private float _startGameSplashTime = 8f;
    private float _startGameDelay = 0f;
    private bool _starting = false;

    private MenuState _currentState = MenuState.MainMenu;

    public GameObject LeftHand;
    public GameObject RightHand;

    // Use this for initialization
    void Awake()
    {
        musicManager = GetComponent<MusicManager>();
        SettingsCanvasManager settingsCanvas = SettingsCanvas.GetComponent<SettingsCanvasManager>();
        settingsCanvas.OnSettingsExit += new SettingsCanvasManager.exitMenuClicked(CloseSettings);
        GameSettings.UpdateLeaderboardDisplay(VRLeaderboard);
        text = GetComponent<Text>();
    }

    void Update()
    {
        switch (_currentState)
        {
            case MenuState.MainMenu:
                {
                    MainMenuUpdate();
                    return;
                }
            case MenuState.Lobby:
                {
                    LobbyUpdate();
                    return;
                }
            case MenuState.Settings:
                {
                    SettingsUpdate();
                    return;
                }
            default:
                {
                    // TODO: this should not be reached, maybe throw error
                    return;
                }
        }
    }

    private void SettingsUpdate()
    {
        var oButtonPressed = Input.GetButtonDown("P0_O");
        var exitSettingsMenu = Input.GetButtonDown("Share") || oButtonPressed;
        if (exitSettingsMenu)
        {
            CloseSettings();
            return;
        }

        _activePlayers.ForEach(player => Kokonut.Disable(player));
        SettingsCanvas.SetActive(true);

    }

    private void CloseSettings()
    {
        SettingsCanvas.GetComponent<SettingsCanvasManager>().ResetCanvas();
        SettingsCanvas.SetActive(false);
        _activePlayers.ForEach(player => Kokonut.Disable(player, false));
        _currentState = MenuState.Lobby;
    }

    private void findHands()
    {
        if (LeftHand != null)
        {
            try
            {
                GameObject handL = LeftHand.transform.Find("LeftRenderModel Slim(Clone)/controller(Clone)").gameObject;
                if (handL != null)
                {
                    handL.SetActive(false);
                }
            }
            catch
            {
                print("Left hand not found");
            }
        }
        if (RightHand != null)
        {
            try
            {
                GameObject handR = RightHand.transform.Find("RightRenderModel Slim(Clone)/controller(Clone)").gameObject;
                if (handR != null)
                {
                    handR.SetActive(false);
                }
            }
            catch
            {
                print("Right hand not found");
            }
        }
    }

    private void LobbyUpdate()
    {
        findHands();

        GameSettings.UpdateLeaderboardDisplay(VRLeaderboard);
        _activePlayers = players.Where(player => player.activeInHierarchy).ToList();
        _activePlayers.ForEach(player =>
        {
            var motion = player.GetComponent<Motion>();
            if (motion.enabled == false)
            {
                motion.enabled = true;
                motion.moveSpeed = 5;
            }
        });

        for (int i = 1; i < 5; i++)
        {
            if (Input.GetButtonDown("P" + i + "_X"))
            {
                // If set controller list doesn't contain current controller
                // Find first inactive Kokonut and assign controller to current controller
                if (!GameSettings.ControllerSet.ContainsKey(i))
                {
                    for (int j = 0; j < players.Length; j++)
                    {
                        if (!players[j].activeSelf)
                        {
                            var kokonut = players[j].GetComponent<Kokonut>();
                            GameSettings.ControllerSet.Add(i, kokonut.GetID());
                            players[j].SetActive(true);
                            kokonut.SetControllerID(i);
                            kokonut.PlayLaugh();
                            break;
                        }
                    }
                }
            }
            if (!_starting)
            {
                if (Input.GetButtonDown("P" + i + "_Tri"))
                {
                    if (GameSettings.ControllerSet.ContainsKey(i))
                    {
                        GameSettings.Invert["P" + i] = !GameSettings.Invert["P" + i];

                        if (GameSettings.ControllerSet[i] == "P1")
                        {
                            GameSettings.Invert["P0"] = !GameSettings.Invert["P0"];
                        }

                        if (GameSettings.Invert["P" + i])
                        {
                            InvertCanvas.transform.Find(GameSettings.ControllerSet[i] + "Invert").GetComponent<Text>().text = "Inverted!";
                        }
                        else
                        {
                            InvertCanvas.transform.Find(GameSettings.ControllerSet[i] + "Invert").GetComponent<Text>().text = "";
                        }
                    }
                }
            }
        }

        numPlayers = _activePlayers.Count;
        GameSettings.numPlayers = numPlayers;

        if (numPlayers > 0)
        {
            if (numPlayers == 4)
            {
                PressXToJoinText.gameObject.SetActive(false);
            }

            if (!_starting)
            {
                text.text = "Press       to Begin!";
                transform.Find("Options").gameObject.SetActive(true);
                if (Input.GetButtonDown("Submit"))
                {
                    _starting = true;
                }
            }
            else
            {
                _startGameTimer -= Time.deltaTime;
                PressXToJoinText.gameObject.SetActive(false);
                text.text = "Starting!";
                transform.Find("Options").gameObject.SetActive(false);
                musicManager.fadeOut();
                VRUIAnimator.SetTrigger("FadeIn");
                if (_startGameTimer <= _startGameSplashTime)
                {
                    InvertCanvas.transform.Find("Objective").gameObject.SetActive(true);
                }
                var preRoundTimerOver = _startGameTimer <= _startGameDelay;
                if (preRoundTimerOver)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                }
            }
        }

        if (!_starting)
        {
            if (Input.GetButtonDown("Share"))
            {
                _currentState = MenuState.Settings;
            }
            if (Input.GetButtonDown("Cancel"))
            {
                if (Leaderboard.activeSelf)
                {
                    Leaderboard.SetActive(false);
                }
                else
                {
                    Leaderboard.SetActive(true);
                    GameSettings.UpdateLeaderboardDisplay(Leaderboard);
                }
            }

            if (Leaderboard.activeSelf)
            {
                if (Input.GetButtonDown("P0_O") && Input.GetButtonDown("P0_SQ"))
                {
                    GameSettings.ClearLeaderboard();
                    GameSettings.UpdateLeaderboardDisplay(Leaderboard);
                    GameSettings.UpdateLeaderboardDisplay(VRLeaderboard);
                }
            }
        }
    }

    private void MainMenuUpdate()
    {
        findHands();

        GameSettings.UpdateLeaderboardDisplay(VRLeaderboard);
        Animator MenuTextAnimator = MainMenuCanvas.GetComponent<Animator>();

        UpdateCurrentSettingState();
        switch (currentMenuSetting)
        {
            case MenuSelectStates.StartLobby:
                {
                    MenuTextAnimator.SetTrigger("LbyTrigger");
                    MainMenuCanvas.transform.Find("Lobby/Stars").gameObject.SetActive(true);
                    MainMenuCanvas.transform.Find("HowToPlay/Stars").gameObject.SetActive(false);
                    MainMenuCanvas.transform.Find("Quit/Stars").gameObject.SetActive(false);
                    HowToPlayCanvas.SetActive(false);

                    var buttonPressed = false;
                    buttonPressed |= Input.GetButtonDown("P0_X");
                    if (buttonPressed)
                    {
                        MainMenuCanvas.SetActive(false);
                        LobbyCanvas.SetActive(true);
                        InvertCanvas.SetActive(true);
                        featherPickUp.SetActive(true);

                        GetComponentInParent<Animator>().SetTrigger("LobbyTrigger");
                        _currentState = MenuState.Lobby;
                    }
                        
                    break;
                }
            case MenuSelectStates.HowToPlay:
                {
                    MenuTextAnimator.SetTrigger("HtpTrigger");
                    MainMenuCanvas.transform.Find("Lobby/Stars").gameObject.SetActive(false);
                    MainMenuCanvas.transform.Find("HowToPlay/Stars").gameObject.SetActive(true);
                    MainMenuCanvas.transform.Find("Quit/Stars").gameObject.SetActive(false);

                    var buttonPressed = false;
                    if (!HowToPlayCanvas.activeSelf)
                    {
                        buttonPressed |= Input.GetButtonDown("P0_X");
                        if (buttonPressed)
                            HowToPlayCanvas.SetActive(true);
                    }
                    else
                    {
                        var dpadX = Input.GetAxisRaw("DpadX");
                        var pressed = AxisAsButton(dpadX, ref xAxisInUse);
                        var increase = dpadX == 1;

                        if (pressed)
                        {
                            if (increase)
                            {
                                HowToPlayCanvas.transform.Find("Controls").gameObject.SetActive(false);
                                HowToPlayCanvas.transform.Find("Objective").gameObject.SetActive(true);
                            }
                            else
                            {
                                HowToPlayCanvas.transform.Find("Controls").gameObject.SetActive(true);
                                HowToPlayCanvas.transform.Find("Objective").gameObject.SetActive(false);
                            }
                        }

                        buttonPressed |= Input.GetButtonDown("P0_O");
                        if (buttonPressed)
                        {
                            HowToPlayCanvas.SetActive(false);
                            HowToPlayCanvas.transform.Find("Controls").gameObject.SetActive(true);
                            HowToPlayCanvas.transform.Find("Objective").gameObject.SetActive(false);
                        }
                    }

                    break;
                }
            case MenuSelectStates.Quit:
                {
                    MenuTextAnimator.SetTrigger("QuitTrigger");
                    MainMenuCanvas.transform.Find("Lobby/Stars").gameObject.SetActive(false);
                    MainMenuCanvas.transform.Find("HowToPlay/Stars").gameObject.SetActive(false);
                    MainMenuCanvas.transform.Find("Quit/Stars").gameObject.SetActive(true);
                    HowToPlayCanvas.SetActive(false);

                    var buttonPressed = false;
                    buttonPressed |= Input.GetButtonDown("P0_X");
                    if (buttonPressed)
                        Application.Quit();
                    break;
                }
        }
    }

    private bool UpdateCurrentSettingState()
    {
        var dpadY = Input.GetAxisRaw("DpadY");
        var pressed = AxisAsButton(dpadY, ref yAxisInUse);
        if (!pressed) return false;
        var up = dpadY == -1;
        currentMenuSetting = up ? currentMenuSetting.Next() : currentMenuSetting.Prev();
        return true;
    }

    private bool AxisAsButton(float axisRaw, ref bool axisLock)
    {
        var pressed = axisRaw != 0;
        var axisReset = axisRaw == 0;
        if (axisLock && axisReset)
        {
            axisLock = false;
            return false;
        }

        if (axisLock && pressed)
        {
            // axis is in use
            return false;
        }

        if (!pressed) return false;
        axisLock = true;
        return true;
    }
}
