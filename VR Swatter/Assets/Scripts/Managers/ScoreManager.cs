﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{

    private static int KokoKingScore;

    // Use this for initialization
    void Start()
    {
        KokoKingScore = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static void ResetScore()
    {
        KokoKingScore = 0;
    }

    public static void UpdateScore(int score)
    {
        KokoKingScore += score;
    }

    public static int GetKokoKingScore()
    {
        return KokoKingScore;
    }
}
