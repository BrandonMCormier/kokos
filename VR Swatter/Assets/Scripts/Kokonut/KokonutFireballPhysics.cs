using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KokonutFireballPhysics : MonoBehaviour
{

    public float SmackPower;
    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 2);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Kokoking")
        {
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }

        if (collision.gameObject.tag == "Fireball")
        {
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }

        if (collision.gameObject.name == "Terrain" || collision.gameObject.tag == "Floor" || collision.gameObject.tag == "Collider")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "Kokonut")
        {
            if (collision.relativeVelocity.magnitude > 10)
            {
                collision.rigidbody.AddForce(rb.velocity * SmackPower);
                Kokonut kokonut = collision.gameObject.GetComponent<Kokonut>();
                if (!kokonut.IsSmacked())
                {
                    kokonut.HitByKokonut();
                    kokonut.StartDisabledTimer();
                    // Can add kokonut hit sound here.
                }
            }
        }
    }
}
