﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallSpawn : MonoBehaviour {

    public GameObject Fireball;
    public Transform Spawnpoint;
    public float speed;

    private bool Shooting = false;
    private float fireTimer = 0f;
    private float fireDelay = 0.1f;

    private float firstShotTimer = 0f;
    private float firstShotDelay = 0.5f;

    private bool firstFired = false;
    private Vector3 velocity;

    private string PlayerName = "";

    // Use this for initialization
    void Start () {
        Kokonut kokonut = transform.parent.gameObject.GetComponent<Kokonut>();

        foreach (KeyValuePair<int, string> entry in GameSettings.ControllerSet)
        {
            if (entry.Value == kokonut.GetID())
            {
                PlayerName = "P" + entry.Key.ToString();
            }
        }
    }

    private String PlayerControl(string control)
    {
        if (PlayerName == "")
        {
            PlayerName = "P0";
        }
        return PlayerName + "_" + control;
    }

    // Update is called once per frame
    void Update () {
        if (firstFired)
        {
            firstShotTimer += Time.deltaTime;
        }

        if (firstShotTimer >= firstShotDelay)
        {
            firstFired = false;
            firstShotTimer = 0f;
        }

        if (Input.GetButtonDown(PlayerControl("R1")))
        {
            Shooting = true;
            if (!firstFired)
            {
                Shoot();
                firstFired = true;
                firstShotTimer = 0f;
            }
        }
        if (Input.GetButtonUp(PlayerControl("R1")))
        {
            Shooting = false;
            fireTimer = 0f;
        }

        if (Shooting)
        {
            if (fireTimer < fireDelay)
            {
                fireTimer += Time.deltaTime;
            }
            if (fireTimer >= fireDelay)
            {
                Shoot();
                fireTimer = 0f;
                firstShotTimer = 0f;
            }
        }

    }
    void Shoot()
    {
        GameObject fireball = Instantiate(Fireball, Spawnpoint.position, Spawnpoint.rotation);
        Rigidbody rb = fireball.GetComponent<Rigidbody>();
        fireball.layer = gameObject.layer;
        rb.velocity = Spawnpoint.forward * speed;
    }
}

