﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuFeatherSpawn : MonoBehaviour {

    public GameObject feather;
    private int[] x_range = { -7, 7 };
    private float y = 2.38f;
    private float z = -1.5f;

    private float respawncounter = 0f;
    private float respawntime = 1;

    // Update is called once per frame
    void Update () {
		if (!feather.activeSelf)
        {
            respawncounter += Time.deltaTime;
            if (respawncounter >= respawntime)
            {
                feather.SetActive(true);
                respawncounter = 0f;
            }
            RandomPosition(feather);
        }
        else
        {
            respawncounter = 0f;
        }
	}

    private void RandomPosition(GameObject feather)
    {
        int positionx = Random.Range(x_range[0], x_range[1]);

        feather.transform.SetPositionAndRotation(new Vector3(positionx, y, z), transform.rotation);
    }
}
