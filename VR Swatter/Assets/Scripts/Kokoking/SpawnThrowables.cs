﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class SpawnThrowables : MonoBehaviour {

    public GameObject throwable;
    public Rigidbody attachPoint;

    [SteamVR_DefaultAction("Interact")]
    public SteamVR_Action_Boolean spawn;

    SteamVR_Behaviour_Pose trackedObj;
    FixedJoint joint;

    private void Awake()
    {
        trackedObj = GetComponent<SteamVR_Behaviour_Pose>();
    }

    private void FixedUpdate()
    {
        if (joint == null && spawn.GetStateDown(trackedObj.inputSource))
        {
            var go = Instantiate(throwable);
            go.transform.position = attachPoint.transform.position;
            go.GetComponent<Interactable>().attachedToHand = gameObject.GetComponent<Hand>();
            joint = go.AddComponent<FixedJoint>();
            joint.connectedBody = attachPoint;
        }
        else if (joint != null && spawn.GetStateUp(trackedObj.inputSource))
        {
            var go = joint.gameObject;
            var rigidbody = go.GetComponent<Rigidbody>();
            DestroyImmediate(joint);
            joint = null;
            go.GetComponent<Interactable>().attachedToHand = null;
            Destroy(go, 15.0f);
            
            var origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;
            
            if (origin != null)
            {
                rigidbody.velocity = origin.TransformVector(trackedObj.GetVelocity());
                rigidbody.angularVelocity = origin.TransformVector(trackedObj.GetAngularVelocity());
            }
            /*
            else
            {
                rigidbody.velocity = trackedObj.GetVelocity();
                rigidbody.angularVelocity = trackedObj.GetAngularVelocity();
            }

            rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
            */
        }
    }
}
